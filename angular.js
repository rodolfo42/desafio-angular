var app = angular.module("app", ['truncate']);
app.factory('_', function() {
  return window._;
});

app.controller("PostsController", function($scope, $interval, $http, _){
  $http.get('http://www.reddit.com/r/EarthPorn/new.json').success(function(posts){
    $scope.posts = posts.data.children;
  });
  $scope.maxChars = 30;
  $scope.favorito = function(post) {
    return !$scope.naoFavorito(post);
  };
  $scope.naoFavorito = function(post) {
    return typeof post.favorito == 'undefined' || post.favorito == 0;
  };
});
